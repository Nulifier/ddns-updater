#######################################
# Builder

FROM golang:alpine AS builder

RUN apk update && apk add --no-cache git ca-certificates && update-ca-certificates

WORKDIR /work
COPY . .

# Download source code for depenedencies
RUN go mod download

# Build the binary
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags "-s -w" ./cmd/ddns-updater

#######################################
# Actual Image

FROM scratch

# Copy Certificats
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# Copy Executable
COPY --from=builder /work/ddns-updater /ddns-updater

ENTRYPOINT ["/ddns-updater"]
