package main

import (
	"context"
	"flag"
	"log"
	"os"

	"github.com/cloudflare/cloudflare-go"
	"gitlab.com/nulifier/ddns-updater/internal/dnscloudflare"
	"gitlab.com/nulifier/ddns-updater/internal/utils"
)

func printUsage() {
	println("Usage: ddns-updater [OPTION]... DOMAIN")
	flag.PrintDefaults()
}

func main() {
	var ipv6 bool
	flag.BoolVar(&ipv6, "ipv6", false, "Use IPv6 records and IP addresses")

	flag.Parse()

	domain := flag.Arg(0)
	zone, err := utils.ExtractDnsZone(domain)
	if err != nil {
		log.Fatal(err)
	}

	// Check if we have enough parameters
	if (domain == "") {
		printUsage()
		os.Exit(1)
	}
	
	api, err := dnscloudflare.NewDnsApi()
	if err != nil {
		log.Fatal(err)
	}

	// Create a context
	ctx := context.Background()

	// Get the current DNS record
	var record cloudflare.DNSRecord
	if ipv6 {
		record, err = api.GetDnsRecord(ctx, zone, domain, "AAA")
	} else {
		record, err = api.GetDnsRecord(ctx, zone, domain, "A")
	}
	if err != nil {
		log.Fatal(err)
	}

	// Get the DNS IP address
	dnsIp := record.Content

	// Get the current IP address
	actualIp, err := utils.GetPublicIp(ipv6)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("DNS IP: '%s', actual IP: '%s', ", dnsIp, actualIp)
	if (dnsIp != actualIp) {
		log.Println("IP does not match, updating...")

		// Update the DNS record
		record.Content = actualIp
		err = api.UpdateDnsRecord(ctx, record)
		if err != nil {
			log.Fatal(err)
		}

		log.Println("Updated DNS record")
	} else {
		log.Println("IP matches, no action needed")
	}
}
