package dnscloudflare

import (
	"context"
	"errors"
	"os"

	"github.com/cloudflare/cloudflare-go"
)

type Credentials struct {
	apiKey string
	apiToken string
	apiEmail string
}

func NewCredentials() (creds Credentials, err error) {
	creds = Credentials{
		apiKey: os.Getenv("CLOUDFLARE_API_KEY"),
		apiToken: os.Getenv("CLOUDFLARE_API_TOKEN"),
		apiEmail: os.Getenv("CLOUDFLARE_API_EMAIL"),
	}

	// If CLOUDFLARE_API_KEY is defined then CLOUDFLARE_API_EMAIL is required
	if (creds.apiKey != "") && (creds.apiEmail == "") {
		return creds, errors.New("Must define CLOUDFLARE_API_EMAIL when using api keys")
	}

	// Either CLOUDFLARE_API_KEY or CLOUDFLARE_API_TOKEN are allowed
	if !((creds.apiKey != "") || (creds.apiToken != "")) {
		return creds, errors.New("Must define either CLOUDFLARE_API_KEY or CLOUDFLARE_API_TOKEN")
	}

	return creds, nil
}

type DnsApi struct {
	creds Credentials
	api *cloudflare.API
}

func NewDnsApi() (dnsApi DnsApi, err error) {
	// Get the credentials
	creds, err := NewCredentials()
	if err != nil {
		return
	}

	// Create an API instance
	var api *cloudflare.API
	if creds.apiToken != "" {
		api, err = cloudflare.NewWithAPIToken(creds.apiToken)
	} else if creds.apiKey != "" {
		api, err = cloudflare.New(creds.apiKey, creds.apiEmail)
	}
	if err != nil {
		return
	}

	return DnsApi{creds, api}, nil
}

func (d DnsApi) GetDnsRecord(ctx context.Context, zoneName string, recordName string, recordType string) (record cloudflare.DNSRecord, err error) {
	// Get zone ID
	zoneID, err := d.api.ZoneIDByName(zoneName)
	if err != nil {
		return cloudflare.DNSRecord{}, err
	}

	// Filter records by name and type
	filter := cloudflare.DNSRecord{Name: recordName, Type: recordType}
	records, err := d.api.DNSRecords(ctx, zoneID, filter)
	if err != nil {
		return cloudflare.DNSRecord{}, err
	}

	if len(records) > 1 {
		return cloudflare.DNSRecord{}, errors.New("more than one record found")
	} else if (len(records) == 0) {
		return cloudflare.DNSRecord{}, errors.New("no records found")
	}

	return records[0], nil
}

func (d DnsApi) UpdateDnsRecord(ctx context.Context, record cloudflare.DNSRecord) error {
	return d.api.UpdateDNSRecord(ctx, record.ZoneID, record.ID, record)
}
