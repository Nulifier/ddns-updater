package utils

import (
	"errors"
	"regexp"
)

func ExtractDnsZone(hostname string) (zone string, err error) {
	re := regexp.MustCompile(`^(?:([a-z0-9-.]+)\.)?([a-z0-9-]+\.[a-z0-9-]+)$`)
	matches := re.FindStringSubmatch(hostname)
	if matches == nil {
		return "", errors.New("cannot find parse hostname for zone")
	}
	return matches[2], nil
}
