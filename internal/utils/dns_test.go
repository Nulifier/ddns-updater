package utils

import "testing"

func TestExtractDnsZone(t *testing.T) {
	zone, err := ExtractDnsZone("example.com")
	if (err != nil) || (zone != "example.com") {
		t.Errorf("ExtractDnsZone(\"example.com\") = %s; want \"example.com\"", zone)
	}

	zone, err = ExtractDnsZone("test.example.com")
	if (err != nil) || (zone != "example.com") {
		t.Errorf("ExtractDnsZone(\"example.com\") = %s; want \"example.com\"", zone)
	}

	zone, err = ExtractDnsZone("long.test.example.com")
	if (err != nil) || (zone != "example.com") {
		t.Errorf("ExtractDnsZone(\"example.com\") = %s; want \"example.com\"", zone)
	}

	zone, err = ExtractDnsZone("notarealzone")
	if (err == nil) {
		t.Error("ExtractDnsZone(\"notarealzone\") shoudl have errored")
	}
}
