package utils

import (
	"io"
	"net/http"
)

// Gets the public IPv4 address of the current connection
func GetPublicIp(useIPv6 bool) (address string, err error) {
	var resp *http.Response
	if useIPv6 {
		resp, err = http.Get("https://api6.ipify.org/")
	} else {
		resp, err = http.Get("https://api4.ipify.org/")
	}
	
	if err != nil {
		return
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return
	}

	return string(body), nil
}
